//
//  Post.swift
//  Facebook-Homework
//
//  Created by Hoeng Linghor on 12/1/20.
//

import Foundation
import UIKit

struct Post {
    var user: String
    var caption: String
    var image: String
    var amountLike: String
    var amountComment: String
    var amountShare: String
    
    init(user: String, caption: String, image: String, amountLike: String, amountComment: String, amountShare: String) {
        self.user = user
        self.caption = caption
        self.image = image
        self.amountLike = amountLike
        self.amountComment = amountComment
        self.amountShare = amountShare
    }
}
