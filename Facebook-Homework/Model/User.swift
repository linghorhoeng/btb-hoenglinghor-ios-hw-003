//
//  User.swift
//  Facebook-Homework
//
//  Created by Hoeng Linghor on 12/1/20.
//

import Foundation
import UIKit

struct User {
    var username: String
    var profile: UIImage
    
    init(username: String, profile: UIImage) {
        self.username = username
        self.profile = profile
    }
}
