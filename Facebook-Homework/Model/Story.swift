//
//  Story.swift
//  Facebook-Homework
//
//  Created by Hoeng Linghor on 12/1/20.
//

import Foundation
import UIKit

struct Story {
    var user: String
    var imageStory: UIImage
    var profileStory: UIImage
    init(user: String, imageStory: UIImage,profileStory: UIImage) {
        self.user = user
        self.imageStory = imageStory
        self.profileStory = profileStory
    }
}
