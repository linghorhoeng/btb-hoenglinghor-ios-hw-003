//
//  StoryCollectionViewCell.swift
//  Facebook-Homework
//
//  Created by Hoeng Linghor on 12/1/20.
//

import UIKit

class StoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameStory: UILabel!
    @IBOutlet weak var imgProfileStory: UIImageView!
    @IBOutlet weak var imgStory: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func data( username: String, profileStory: UIImage, imageStory: UIImage) {
        self.nameStory?.text = username
        self.imgProfileStory?.image = profileStory
        self.imgStory?.image = imageStory
    }
}
