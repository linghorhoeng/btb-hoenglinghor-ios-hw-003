//
//  PostTableViewCell.swift
//  Facebook-Homework
//
//  Created by Hoeng Linghor on 11/30/20.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var share: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var likeTotal: UILabel!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var hour: UILabel!
    @IBOutlet weak var namePost: UILabel!
    @IBOutlet weak var profilePost: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func data(username: String, postProfile: UIImage, caption: String, imagePost: UIImage, like: String, comment: String, share: String) {
        self.namePost?.text = username
        self.profilePost?.image = postProfile
        self.caption?.text = caption
        self.imgPost?.image = imagePost
        self.likeTotal?.text = like
        self.comment?.text = comment
        self.share?.text = share
    }
}
