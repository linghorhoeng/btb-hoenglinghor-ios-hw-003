//
//  StatusTableViewCell.swift
//  Facebook-Homework
//
//  Created by Hoeng Linghor on 12/1/20.
//

import UIKit

class StatusTableViewCell: UITableViewCell {

    @IBOutlet weak var share: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var like: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var hour: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func data(username: String, profileStatus: UIImage,  statusPost: String, like: String, comment: String, share: String) {
        self.name?.text = username
        self.imgProfile?.image = profileStatus
        self.status?.text = statusPost
        self.like?.text = like
        self.comment?.text = comment
        self.share?.text = share
    }
}
