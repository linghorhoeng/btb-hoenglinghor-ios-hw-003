//
//  ViewController.swift
//  Facebook-Homework
//
//  Created by Hoeng Linghor on 11/30/20.
//

import UIKit

//private let STORY_CELL = "StoryCollectionViewCell"

class ViewController: UIViewController {
    var stories: [Story]?
    var posts: [Post]?
    var users: [User]?

    @IBOutlet weak var storyView: UICollectionView!
    @IBOutlet weak var tbView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        registerCell()
        bindData()
    }
    private func bindData() {
        let post1 = Post(user: "Mama", caption: "If you’re in need of motivation and inspiration, these life quotes from Hoda Kotb, Meghan Markle, Reese Witherspoon, among others, are exactly what you need to hear. Advice from practicing self-love to having a sense of humor to being optimistic never grows old and will surely help you find the push you need to get where you’re going.", image: "ice1", amountLike: "70k", amountComment: "300 comments", amountShare: "10k shares")
        let post2 = Post(user: "Nytali", caption: "Peace can challenge us to discover ourselves and perhaps, allow us to surrender to our circumstances. When you stop going against the grain and going with the flow, life might just get a little sweeter. But on those days when peace seems so out of reach, even with the help of meditation, center yourself with these peace quotes that’ll remind you that achieving tranquility is an ongoing journey.", image: "ice2", amountLike: "10k", amountComment: "500 comments", amountShare: "7k shares")
        let post3 = Post(user: "suly", caption: "Peace can challenge us to discover ourselves and perhaps, allow us to surrender to our circumstances. When you stop going against the grain and going with the flow, life might just get a little sweeter. But on those days when peace seems so out of reach, even with the help of meditation, center yourself with these peace quotes that’ll remind you that achieving tranquility is an ongoing journey.", image: "", amountLike: "12k", amountComment: "900 comments", amountShare: "7k shares")
        let post4 = Post(user: "Waiwai", caption: "You should feel beautiful and you should feel safe. What you surround yourself with should bring you peace of mind and peace of spirit. —Stacy London"
            , image: "ton2", amountLike: "10k", amountComment: "10k comments", amountShare: "7k shares")
        let post5 = Post(user: "Arura", caption: "Sometimes you can find peace of mind by transferring yourself to different situations. They're just reminders to stay ... calm.", image: "panda", amountLike: "11k", amountComment: "800 comments", amountShare: "70k shares")
        let post6 = Post(user: "Vanila", caption: "Let go of the thoughts that don’t make you strong.", image: "my3", amountLike: "12k", amountComment: "900 comments", amountShare: "7k shares")
        let post7 = Post(user: "suly", caption: "Peace can challenge us to discover ourselves and perhaps, allow us to surrender to our circumstances. When you stop going against the grain and going with the flow, life might just get a little sweeter. But on those days when peace seems so out of reach, even with the help of meditation, center yourself with these peace quotes that’ll remind you that achieving tranquility is an ongoing journey.", image: "", amountLike: "12k", amountComment: "900 comments", amountShare: "7k shares")
        let post8 = Post(user: "suly", caption: "Peace can challenge us to discover ourselves and perhaps, allow us to surrender to our circumstances. When you stop going against the grain and going with the flow, life might just get a little sweeter. But on those days when peace seems so out of reach, even with the help of meditation, center yourself with these peace quotes that’ll remind you that achieving tranquility is an ongoing journey.", image: "my2", amountLike: "12k", amountComment: "900 comments", amountShare: "7k shares")


        let story1 = Story(user: "vanila", imageStory:UIImage(named: "my3")!, profileStory: UIImage(named: "my3")!)
        let story2 = Story(user: "lily", imageStory:UIImage(named: "my1")!, profileStory: UIImage(named: "my1")!)
        let story3 = Story(user: "Adda", imageStory:UIImage(named: "pan2")!, profileStory: UIImage(named: "pan3")!)
        let story4 = Story(user: "Bambi", imageStory:UIImage(named: "pan3")!, profileStory: UIImage(named: "pan2")!)
        let story5 = Story(user: "Coosta", imageStory:UIImage(named: "ton5")!, profileStory: UIImage(named: "we1")!)
        let story6 = Story(user: "Namtan", imageStory:UIImage(named: "ton6")!, profileStory: UIImage(named: "panda")!)
        let story7 = Story(user: "Supart", imageStory:UIImage(named: "ton7")!, profileStory: UIImage(named: "my4")!)
        let story8 = Story(user: "Tonton", imageStory:UIImage(named: "pan1")!, profileStory: UIImage(named: "my3")!)

        let user1 = User(username: "Jane", profile:UIImage(named: "ton4")!)
        let user2 = User(username: "Kevin", profile: UIImage(named: "we1")!)
        let user3 = User(username: "William", profile: UIImage(named: "ice1")!)
        let user4 = User(username: "Quiqui", profile: UIImage(named: "pan2")!)
        let user5 = User(username: "Shui", profile: UIImage(named: "pan3")!)
        let user6 = User(username: "Tata", profile: UIImage(named: "my2")!)
        let user7 = User(username: "Kaka", profile: UIImage(named: "my4")!)
        let user8 = User(username: "Jojo", profile: UIImage(named: "ton3")!)
        
        posts = [post1,post2,post3,post4,post5,post6,post7,post8]
        stories = [story1,story2,story3,story4,story5,story6,story7,story8]
        users = [user1,user2,user3,user4,user5,user6,user7,user8]
//        users!.append(contentsOf: [user1, user2, user3, user4, user5, user6, user7])
//        posts!.append(contentsOf: [post1, post2, post3, post4, post5, post6,post7,post8])

    }
    func registerCell(){
//        let nib = UINib(nibName: STORY_CELL, bundle: nil)
//        storyView.register(nib, forCellWithReuseIdentifier: STORY_CELL)
        storyView.register(UINib(nibName: "StoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "StoryCollectionViewCell")
        tbView.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "postCell")
        tbView.register(UINib(nibName: "StatusTableViewCell", bundle: nil), forCellReuseIdentifier: "statusCell")
    }
}
extension ViewController: UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stories!.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoryCollectionViewCell", for: indexPath) as! StoryCollectionViewCell
        let profileStory = stories![indexPath.row].profileStory
        let storyImage = stories![indexPath.row].imageStory
        let username = stories![indexPath.row].user
        cell.data(username: username, profileStory: profileStory, imageStory: storyImage)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 4, height: collectionView.frame.height)
        }
    
}

extension ViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts?.count ?? 0    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostTableViewCell
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "statusCell", for: indexPath) as! StatusTableViewCell
        let username = users![indexPath.row].username
        let profile = users![indexPath.row].profile
        let caption = posts![indexPath.row].caption
        let imagePost = UIImage(named: posts![indexPath.row].image)
        let like = posts![indexPath.row].amountLike
        let comment = posts![indexPath.row].amountComment
        let share = posts![indexPath.row].amountShare
        
        if let imagePost = imagePost {
            cell.data(username: username, postProfile: profile, caption: caption, imagePost: imagePost, like: like, comment: comment, share: share)
            return cell
        }else{
            cell1.data(username: username, profileStatus: profile, statusPost: caption, like: like, comment: comment, share: share)
            return cell1
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//
//        return 100
//    }
}
